---
  title: What is DevOps?
  description: Learn about the benefits and features of the DevOps lifecycle, and how to use communication and collaboration to deliver better quality code, faster!
  components:
    - name: topics-header
      data:
        title: What is DevOps?
        read_time: 8 min read
        updated_date: 5.23.22
        block:
            - text: |
                  If you want to build better software faster, DevOps is the answer.  Here’s how this software development methodology brings everyone to the table to create secure code quickly.
              link_text: "Download the DevOps Ebook Now"
              link_href: https://page.gitlab.com/resources-ebook-beginners-guide-devops.html
    - name: topics-copy-block
      data:
        header: Overview
        column_size: 10
        blocks:
          - text: |
              DevOps combines development and operations to increase the efficiency, speed, and security of software development and delivery compared to traditional processes. A more nimble software development lifecycle results in a competitive advantage for businesses and their customers.
            image:
              image_url: /nuxt-images/topics/devops-lifecycle.svg
              alt: Continuous integration
    - name: topics-copy-block
      data:
        header: DevOps explained
        column_size: 10
        blocks:
          - text: DevOps can be best explained as people working together to conceive, build and deliver secure software at top speed. DevOps practices enable software developers (devs) and operations (ops) teams to accelerate delivery through automation, collaboration, fast feedback, and iterative improvement.
          - text: Stemming from an [Agile approach](/topics/agile-delivery/){data-ga-name="Agile delivery"}{data-ga-location="body"} to software development, a DevOps delivery process expands on the cross-functional approach of building and shipping applications in a faster and more iterative manner. In adopting a DevOps development process, you are making a decision to improve the flow and value delivery of your application by encouraging a more collaborative environment at all stages of the development cycle.
          - text: DevOps represents a change in mindset for IT culture. In building on top of Agile, lean practices, and systems theory, DevOps focuses on incremental development and rapid delivery of software. Success relies on the ability to create a culture of accountability, improved collaboration, empathy, and joint responsibility for business outcomes.
    - name: topics-emphasis
      data:
        highlight: DevOps
        column_size: 10
        text: |
            is a combination of software developers (dev) and operations (ops). It is defined as a software engineering methodology which aims to integrate the work of software development and software operations teams by facilitating a culture of collaboration and shared responsibility.
    - name: topics-copy-block
      data:
        header: Core DevOps principles
        column_size: 10
        blocks:
            - text: |
                The DevOps methodology comprises [four key principles](/blog/2022/02/11/4-must-know-devops-principles/) that guide the effectiveness and efficiency of application development and deployment. These principles, listed below, center on the best aspects of modern software development.

                1. Automation of the software development lifecycle
                2. Collaboration and communication
                3. Continuous improvement and minimization of waste
                4. Hyperfocus on user needs with short feedback loops

                By adopting these principles, organizations can improve code quality, achieve a faster time to market, and engage in better application planning.
    - name: topics-copy-block
      data:
        header: The four phases of DevOps
        column_size: 10
        blocks:
            - text: |
                As DevOps has evolved, so has its complexity. This complexity is driven by two factors:

                * Organizations are moving from monolithic architectures to [microservices architectures](/topics/microservices/). As DevOps matures, organizations need more and more DevOps tools per project.

                * The result of more projects and more tools per project has been an exponential increase in the number of project-tool integrations. This necessitated a change in the way organizations adopted DevOps tools.

                This evolution took place in following four phases:

                  ### Phase 1: Bring Your Own DevOps

                In the Bring Your Own DevOps phase, each team selected its own tools. This approach caused problems when teams attempted to work together because they were not familiar with the tools of other teams.

                  ### Phase 2: Best-in-class DevOps

                To address the challenges of using disparate tools, organizations moved to the second phase, Best-in-class DevOps. In this phase, organizations standardized on the same set of tools, with one preferred tool for each stage of the DevOps lifecycle. It helped teams collaborate with one another, but the problem then became moving software changes through the tools for each stage.

                  ### Phase 3: Do-it-yourself DevOps

                To remedy this problem, organizations adopted Do-it-yourself (DIY) DevOps, building on top of and between their tools. They performed a lot of custom work to integrate their DevOps point solutions together. However, since these tools were developed independently without integration in mind, they never fit quite right. For many organizations, maintaining DIY DevOps was a significant effort and resulted in higher costs, with engineers maintaining tooling integration rather than working on their core software product.

                  ### Phase 4: DevOps Platform

                A single-application platform approach improves the team experience and business efficiency. GitLab, The DevOps Platform, replaces DIY DevOps, allowing visibility throughout and control over all stages of the DevOps lifecycle.

                By empowering all teams – Software, Operations, IT, Security, and Business – to collaboratively plan, build, secure, and deploy software across an end-to-end unified system, GitLab represents a fundamental step-change in realizing the full potential of DevOps. The DevOps Platform is a single application powered by a cohesive user interface, agnostic of self-managed or SaaS deployment. It is built on a single codebase with a unified data store, that allows organizations to resolve the inefficiencies and vulnerabilities of an unreliable DIY toolchain.

                  As we look ahead to software-led organizations becoming even more distributed and agile, every company will need a DevOps platform to modernize software development and delivery. By making it easier and trusted to adopt the next generation of cloud-native technologies – from microservices to serverless and eventually edge architecture – all companies will be empowered to ship software faster, at maximum efficiency, with security embedded across their end-to-end software supply chain.
    - name: topics-copy-block
      data:
        header: How DevOps can benefit from AI/ML?
        column_size: 10
        blocks:
            - text: |
                AI and machine learning (ML) are still maturing in their applications for DevOps, but there is plenty for organizations to take advantage of today, including using the technology to make sense of test data.

                AI and ML can find patterns, figure out the coding problems that cause bugs, and alert DevOps teams so they can dig deeper.

                Similarly, DevOps teams can use AI and ML to sift through security data from logs and other tools to detect breaches, attacks, and more. Once these issues are found, AI and ML can respond with automated mitigation techniques and alerting.

                AI and ML can save developers and operations professionals time by learning how they work best, making suggestions within workflows, and automatically provisioning preferred infrastructure configurations.
              link:
                url: /blog/2022/02/14/top-10-ways-machine-learning-may-help-devops/
                text: Read more about the benefits of AI and ML for DevOps
                data_ga_name: Read more about the benefits of AI and ML for DevOps
    - name: topics-copy-block
      data:
        header: What is a DevOps platform?
        column_size: 10
        blocks:
          - text: |
                DevOps brings the human siloes together and a [DevOps platform](/solutions/devops-platform/){data-ga-name="Devops platform"}{data-ga-location="body"} does the same thing for tools. Many teams start their [DevOps journey](https://page.gitlab.com/webcast-simplify-devops.html){data-ga-name="Devops journey"}{data-ga-location="body"} with a disparate collection of tools, all of which have to be maintained and many of which don’t or can’t integrate. A DevOps platform brings tools together in a single application for unparalleled collaboration, visibility, and development velocity. A DevOps platform is how modern software should be created, secured, released, and monitored in a repeatable fashion. A true DevOps platform means teams can iterate faster and innovate together because everyone can contribute.
    - name: topics-copy-block
      data:
        header: Benefits of a DevOps Culture
        column_size: 10
        blocks:
          - text: |
              The business value of DevOps and the benefits of a DevOps culture lies in the ability to improve the production environment in order to deliver software faster with continuous improvement. You need the ability to anticipate and respond to industry disruptors without delay. This becomes possible within an Agile software development process where teams are empowered to be autonomous and deliver faster, reducing work in progress. Once this occurs, teams are able to respond to demands at the speed of the market.

              There are some fundamental concepts that need to be put into action in order for DevOps to function as designed, including the need to:


              * Remove institutionalized silos and handoffs that lead to roadblocks and constraints, particularly in instances where the measurements of success for one team is in direct odds with another team’s key performance indicators (KPIs).


              * Implement a unified tool chain using a single application that allows multiple teams to share and collaborate. This will enable teams to accelerate delivery and provide fast feedback to one another.
    - name: topics-copy-block
      data:
        header: What is the goal of DevOps?
        column_size: 10
        blocks:
          - text: |
              DevOps represents a change in mindset for IT culture. In building on top of [Agile](/solutions/agile-delivery/) and lean practices, DevOps focuses on incremental development and rapid delivery of software. Success relies on the ability to create a culture of accountability, improved collaboration, empathy, and joint responsibility for business outcomes.

              Adopting a DevOps strategy enables businesses to increase operational efficiencies, deliver better products faster, and reduce security and compliance risk.
    - name: topics-copy-block
      data:
        header: The DevOps lifecycle and how DevOps works
        column_size: 10
        blocks:
          - text: |
              The [DevOps lifecyle](/devops-tools/) stretches from the beginning of software development through to delivery, maintenance, and security. The 10 lifecycle stages are:
    - name: benefits-icons
      data:
        use_icon_component: true
        column_size: 10
        benefits:
          - title: Manage
            icon:
              name: manage
              alt: manage icon
            text: DevOps closes the loop and incorporates feedback and learnings from the entire lifecycle into your ongoing iteration.
          - title: Plan
            icon:
              name: plan
              alt: plan icon
            text: DevOps describes the work that needs to be done, prioritize it, and track its completion.
          - title: Create
            icon:
              name: create
              alt: create icon
            text: DevOps writes code, proposes changes, and discusses these proposals with coworkers.
          - title: Verify
            icon:
              name: verify
              alt: verify icon
            text: DevOps automatically tests code to make sure it works correctly.
          - title: Package
            icon:
              name: package
              alt: package icon
            text: DevOps stores the software in a state where it can be reused later.
          - title: Secure
            icon:
              name: secure
              alt: secure icon
            text: DevOps checks whether the software contains vulnerabilities through static and dynamic tests, fuzz testing, and dependency scanning.
          - title: Release
            icon:
              name: release
              alt: release icon
            text: DevOps deploys the software to end users.
          - title: Configure
            icon:
              name: configure
              alt: configure icon
            text: DevOps manages infrastructure and software platforms.
          - title: Monitor
            icon:
              name: monitor
              alt: monitor icon
            text: DevOps sees the impact of the software is on infrastructure and users. It also provides data to help effectively respond to incidents.
          - title: Govern
            icon:
              name: protect
              alt: govern icon
            text: Devops manages security vulnerabilities, policies, and compliance across your organization.
        footer: Some organizations string together a series of tools to gain all of this functionality, but that can be incredibly costly and complex to deploy, manage, and maintain.
    - name: topics-copy-block
      data:
        header: DevOps tools, concepts and fundamentals
        column_size: 10
        blocks:
          - text: |
              DevOps covers a wide range of practices across the application lifecycle. Customers start with one or more of these practices in their journey to DevOps success.
    - name: topics-info-table
      data:
        column_size: 10
        rows:
          - title: Source Code Management
            text: Teams looking for better ways to manage changes to documents, software, images, large web sites, and other collections of code, configuration, and metadata among disparate teams.
          - title: Agile Project & Portfolio Management
            text: Teams looking for a better way of initiating, planning, executing, controlling, and closing the work of a team to achieve specific goals and meet specific success criteria at the specified time.
          - title: Continuous Integration (CI)
            text:  Teams looking for ways to automate the build and testing processes to consistently integrate code and continuously test to minimise the manual efforts spent in frequent runs of unit and integration tests.
          - title: Continuous Delivery (CD)
            text: Teams looking for ways to automate the build, test and packaging, configuration and deployment of applications to a target environment.
          - title: Shift Left Security
            text: Teams looking for ways to identify vulnerabilities during development with actionable information to empower dev to remediate vulnerabilities earlier in the lifecycle have specific goals and meet specific success criteria at the specified time.
          - title: Monitoring and Feedback
            text: Teams looking for ways to embed monitoring into every deployed version and the impact of application changes to the business value and user experience.
          - title: Rapid Innovation
            text: Teams looking for ways to provide feedback back into the development, test, packaging & deployment stages to complete the loop to integrate dev and ops teams and provide real time feedback from production environments and customers.
    - name: topics-copy-block
      data:
        header: How does DevSecOps relate to DevOps?
        column_size: 10
        blocks:
          - text: |
              Security has become an integral part of the software development lifecycle, with much of the security shifting left in the development process. [DevSecOps](/blog/2021/06/01/gitlab-is-setting-standard-for-devsecops/) ensures that DevOps teams understand the security and compliance requirements from the very beginning of application creation and can properly protect the integrity of the software.

              By integrating security seamlessly into DevOps workflows, organizations gain the visibility and control necessary to meet complex security demands, including vulnerability reporting and auditing. Security teams can ensure that policies are being enforced throughout development and deployment, including critical testing phases.

              DevSecOps can be implemented across an array of environments such as on-premises, cloud-native, and hybrid, ensuring maximum control over the entire software development lifecycle.
    - name: topics-copy-block
      data:
        header: How is DevOps and CI/CD related?
        column_size: 10
        blocks:
          - text: |
              CI/CD is an essential part of [DevOps](/topics/devops/) and any modern software development practice. A purpose-built CI/CD platform can maximize development time by improving an organization’s productivity, increasing efficiency, and streamlining workflows through built-in automation, testing, and collaboration.

              As applications grow larger, the features of CI/CD can help [decrease development complexity](https://blog-post-parent-child-multi-project-pipelines.about.gitlab-review.app/blog/2022/02/22/parent-child-vs-multi-project-pipelines/). Adopting other DevOps practices — like shifting left on security and creating tighter feedback loops — helps break down development silos, scale safely, and get the most out of CI/CD.
    - name: topics-copy-block
      data:
        header: How does DevOps support the cloud-native approach?
        column_size: 10
        blocks:
          - text: |
              Moving software development to the cloud has so many advantages that more and more companies are adopting cloud-native computing. Building, testing, and deploying applications from the cloud saves money because organizations can scale resources more easily, support faster software shipping, align with business goals, and free up DevOps teams to innovate rather than maintain infrastructure.

              Cloud-native application development enables developers and operations teams to work more collaboratively, which results in better software delivered faster.

            link:
              url: /cloud-native/
              text: Read more about the benefits of cloud-native DevOps environments
              data_ga_name: Read more about the benefits of cloud-native DevOps environments
    - name: topics-copy-block
      data:
        header: What is a DevOps engineer?
        column_size: 10
        blocks:
          - text: |
              A DevOps engineer is responsible for all aspects of the software development lifecycle, including communicating critical information to the business and customers. Adhering to DevOps methodologies and principles, they efficiently integrate development processes into workflows, introduce automation where possible, and test and analyze code. They build, evaluate, deploy, and update tools and platforms (including IT infrastructure if necessary). DevOps engineers manage releases, as well as identify and help resolve technical issues for software users.

              DevOps engineers require knowledge of a range of programming languages and a strong set of communication skills to be able to collaborate among engineering and business groups.
    - name: topics-copy-block
      data:
        header: Benefits of DevOps
        column_size: 10
        blocks:
         - text:  |
            [Adopting a DevOps mode](/customers/axway-devops/){data-ga-name="Axway devops"}{data-ga-location="body"} breaks down barriers so that development and operations teams are no longer siloed and have a more efficient way to work across the entire development and application lifecycle. Without DevOps, organizations experience handoff friction, which delays the delivery of software releases and negatively impacts business results.

            The DevOps model is an organization’s answer to increasing operational efficiency, accelerating delivery, and innovating products. Organizations that have implemented a DevOps culture experience the benefits of increased collaboration, fluid responsiveness, and shorter cycle times.
    - name: benefits-icons
      data:
        use_icon_component: true
        column_size: 10
        benefits:
        - title: Collaboration
          icon:
            name: collaboration
            alt: collaboration icon
          text: |
            Adopting a DevOps model creates alignment between development and operations teams; handoff friction is reduced and everyone is all in on the same goals and objectives.
        - title: Fluid responsiveness
          icon:
            name: cycle
            alt: cycle icon
          text: |
              More collaboration leads to real-time feedback and greater efficiency; changes and improvements can be implemented quicker and guesswork is removed.
        - title: Shorter cycle time
          icon:
            name: stopwatch
            alt: stopwatch icon
          text: |
              Improved efficiency and frequent communication between teams shortens cycle time; new code can be released more rapidly while maintaining quality and security.
    - name: topics-cta
      data:
        title: Start your DevOps journey
        subtitle: Starting and Scaling DevOps in the Enterprise
        icon:
          name: book
          alt: book icon
        text: |
            Sharing his pioneering insight on how organizations can transform their software development and delivery processes, Gary Gruver provides a tactical framework to implement DevOps principles in “Starting and Scaling DevOps in the Enterprise.”
        column_size: 10
        cta_one:
          text: Download your free copy
          link: /resources/scaling-enterprise-devops/
          data_ga_name: Download free copy - Starting and Scaling DevOps in the Enterprise
          data_ga_location: body
    - name: solutions-resource-cards
      data:
        title: Related Resources
        column_size: 4
        grouped: true
        cards:
          - icon:
              name: video
              variant: marketing
              alt: Video Icon
            event_type: "Video"
            header: How DevOps leads transformation (GitLab Virtual Commit 2020 track)
            link_text: "Learn more"
            image: "/nuxt-images/resources/fallback/img-fallback-cards-devops.png"
            href: "https://www.youtube.com/playlist?list=PLFGfElNsQthbAbiHjRVNz1WwxbhLfeXXs"
            data_ga_name: How DevOps leads transformation (GitLab Virtual Commit 2020 track)
            data_ga_location: resource cards
          - icon:
              name: video
              variant: marketing
              alt: Video Icon
            event_type: "Video"
            header: Cloud-Native DevOps (GitLab Virtual Commit 2020 track)
            link_text: "Learn more"
            image: "/nuxt-images/resources/fallback/img-fallback-cards-infinity.png"
            href: https://www.youtube.com/playlist?list=PLFGfElNsQthb4FD4y1UyEzi2ktSeIzLxj
            data_ga_name: Cloud-Native DevOps (GitLab Virtual Commit 2020 track)
            data_ga_location: resource cards
          - icon:
              name: video
              variant: marketing
              alt: Video Icon
            event_type: "Video"
            header: DevOps tips and tricks (GitLab Virtual Commit 2020 track)
            link_text: "Learn more"
            image: "/nuxt-images/resources/fallback/img-fallback-cards-cicd.png"
            href: https://www.youtube.com/playlist?list=PLFGfElNsQthZ_LGh4EpGJduNd2nFhN5fn
            data_ga_name: DevOps tips and tricks (GitLab Virtual Commit 2020 track)
            data_ga_location: resource cards
          - icon:
              name: video
              variant: marketing
              alt: Video Icon
            event_type: "Video"
            header: How to simplify DevOps
            link_text: "Learn more"
            image: "/nuxt-images/resources/fallback/img-fallback-cards-gitlab.png"
            href: https://www.youtube.com/watch?v=TUwvgz-wsF4
            data_ga_name: How to simplify DevOps
            data_ga_location: resource cards
          - icon:
              name: case-study
              variant: marketing
              alt: Case Study Icon
            event_type: "Case study"
            header: Axway aims for elite DevOps status
            link_text: "Learn more"
            image: "/nuxt-images/resources/resources_8.jpeg"
            href: /customers/axway-devops/
            data_ga_name: Axway aims for elite DevOps status
            data_ga_location: resource cards
          - icon:
              name: case-study
              variant: marketing
              alt: Case Study Icon
            event_type: "Case study"
            header: Worldline and the importance of collaboration
            link_text: "Learn more"
            image: "/nuxt-images/resources/resources_3.jpg"
            href: /customers/worldline/
            data_ga_name: Worldline and the importance of collaboration
            data_ga_location: resource cards
          - icon:
              name: case-study
              variant: marketing
              alt: Case Study Icon
            event_type: "Case study"
            header: The European Space Agency and DevOps
            link_text: "Learn more"
            image: "/nuxt-images/resources/resources_1.jpeg"
            href: /customers/european-space-agency/
            data_ga_name: The European Space Agency and DevOps
            data_ga_location: resource cards
          - icon:
              name: report
              variant: marketing
              alt: Report Icon
            event_type: "Report"
            header: "GitLab’s 2022 Global DevSecOps Survey"
            link_text: "Learn more"
            image: "/nuxt-images/resources/resources_11.jpeg"
            href: /developer-survey/
            data_ga_name: "GitLab’s 2020 Global DevSecOps Survey"
            data_ga_location: resource cards
          - icon:
              name: report
              variant: marketing
              alt: Report Icon
            event_type: "Report"
            header: "Gartner on application release orchestration"
            link_text: "Learn more"
            image: "/nuxt-images/resources/resources_14.jpeg"
            href: /blog/2020/01/16/2019-gartner-aro-mq/
            data_ga_name: "Gartner on application release orchestration"
            data_ga_location: resource cards
          - icon:
              name: podcast-alt
              variant: marketing
              alt: Podcast Icon
            event_type: "Podcast"
            header: "Arrested DevOps"
            link_text: "Learn more"
            image: "/nuxt-images/resources/resources_4.jpeg"
            href: https://www.arresteddevops.com/
            data_ga_name: "Arrested DevOps"
            data_ga_location: resource cards
          - icon:
              name: ebook
              variant: marketing
              alt: Ebook Icon
            event_type: "Book"
            header: "Leading the Transformation"
            link_text: "Learn more"
            image: "/nuxt-images/resources/resources_9.jpeg"
            href: https://www.amazon.com/Leading-Transformation-Applying-DevOps-Principles/dp/1942788010
            data_ga_name: "Leading the Transformation"
            data_ga_location: resource cards
          - icon:
              name: ebook
              variant: marketing
              alt: Ebook Icon
            event_type: "Book"
            header: "The Goal"
            link_text: "Learn more"
            image: "/nuxt-images/resources/resources_18.jpg"
            href: https://www.amazon.com/The-Goal-Process-Ongoing-Improvement/dp/0884271951/
            data_ga_name: "The Goal"
            data_ga_location: resource cards
          - icon:
              name: ebook
              variant: marketing
              alt: Ebook Icon
            event_type: "Book"
            header: "Starting and Scaling DevOps in the Enterprise"
            link_text: "Learn more"
            image: "/nuxt-images/resources/resources_5.jpeg"
            href: https://www.amazon.com/Start-Scaling-Devops-Enterprise-Gruver/dp/1483583589/
            data_ga_name: "Starting and Scaling DevOps in the Enterprise"
            data_ga_location: resource cards
          - icon:
              name: ebook
              variant: marketing
              alt: Ebook Icon
            event_type: "Book"
            header: "The Phoenix Project"
            link_text: "Learn more"
            image: "/nuxt-images/resources/resources_16.jpg"
            href: https://www.amazon.com/The-Phoenix-Project-Helping-Business/dp/0988262509/
            data_ga_name: "The Phoenix Project"
            data_ga_location: resource cards
    - name: solutions-resource-cards
      data:
        title: Suggested Content
        column_size: 4
        cards:
          - icon:
              name: web
              variant: marketing
              alt: Web Icon
            event_type: "Web"
            header: "Auto DevOps 101: How we're making CI/CD easier"
            text: |
                  VP of product strategy Mark Pundsack shares everything you need to know about Auto DevOps
            link_text: "Learn more"
            href: /blog/2019/10/07/auto-devops-explained/
            image: /nuxt-images/blogimages/autodevops.jpg
            data_ga_name: "Auto devops"
            data_ga_location: resource cards
          - icon:
              name: web
              variant: marketing
              alt: Web Icon
            event_type: "Web"
            header: "A beginner's guide to continuous integration"
            text: |
                  Here's how to help everyone on your team, like designers and testers, get started with GitLab CI.
            link_text: "Learn more"
            href: /blog/2018/01/22/a-beginners-guide-to-continuous-integration/
            image: /nuxt-images/blogimages/beginners-guide-to-ci.jpg
            data_ga_name: "A beginner's guide to continuous integration"
            data_ga_location: resource cards
          - icon:
              name: web
              variant: marketing
              alt: Web Icon
            event_type: "Web"
            header: "Leading SCM, CI and Code Review in one application"
            text: |
                  The most important tools for developers are SCM, CI and Code Review, and it is better to have them all together.
            link_text: "Learn more"
            href: /blog/2020/09/30/leading-scm-ci-and-code-review-in-one-application
            image: /nuxt-images/blogimages/scm-ci-cr.png
            data_ga_name: "Leading SCM, CI and Code Review in one application"
            data_ga_location: resource cards
          - icon:
              name: web
              variant: marketing
              alt: Web Icon
            event_type: "Web"
            header: "A single application for your end-to-end DevOps needs starts with Version Control & Collaboration"
            text: |
                  Version Control & Collaboration is centered at the core of your end-to-end DevOps single application needs
            link_text: "Learn more"
            href: /blog/2020/10/07/vcc-with-a-single-app/
            image: /nuxt-images/blogimages/markus-spiske-MkwAXj8LV8c-unsplash.png
            data_ga_name: "A single application for your end-to-end DevOps needs starts with Version Control & Collaboration"
            data_ga_location: resource cards
          - icon:
              name: web
              variant: marketing
              alt: Web Icon
            event_type: "Web"
            header: "Future-proof your developer career"
            text: |
                  Roles are changing and AI is coming. We asked 14 DevOps practitioners, analysts, and GitLa execs how to future-proof your career.
            link_text: "Learn more"
            href: /blog/2020/10/30/future-proof-your-developer-career/
            image: /nuxt-images/blogimages/future-of-software-future-proof-your-career.png
            data_ga_name: "Future-proof your developer career"
            data_ga_location: resource cards
          - icon:
              name: web
              variant: marketing
              alt: Web Icon
            event_type: "Web"
            header: "How a GitLab engineer changed the future of DevOps"
            text: |
                  When Kamil Trzciński suggested we integrate GitLab version control and GitLab CI one into a single product, GitLab's pioneering DevOps Platform was born.
            link_text: "Learn more"
            href: /blog/2020/10/29/gitlab-hero-devops-platform/
            image: /nuxt-images/blogimages/whatisgitlabflow.jpg
            data_ga_name: "How a GitLab engineer changed the future of DevOps"
            data_ga_location: resource cards

  schema_faq:
    - question: What is DevOps?
      answer: |
        DevOps can be best explained as people working together to build,
        deliver, and run resilient software at the speed of their particular
        business. DevOps practices enable software development (Dev) and
        operations (Ops) teams to accelerate delivery through automation,
        collaboration, fast feedback, and iterative improvement.


        [Learn more about DevOps](/topics/devops/#what-is-devops:~:text=What%20is%20DevOps%3F)
    - question: What is a DevOps platform?
      answer: |
        DevOps brings the human siloes together and a DevOps platform does the
        same thing for tools.


        [Learn more about DevOps platform](/topics/devops/#what-is-a-devops-platform:~:text=What%20is%20a%20DevOps%20platform%3F)
